import {
	Application as App,
	loader as Loader,
	Rectangle,
	Sprite,
} from 'pixi.js';

const app = new App();
app.renderer.backgroundColor = 0x999999;
document.body.appendChild(app.view);

const ctx = app.renderer.view.getContext('webgl');
PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
app.renderer.autoResize = true;
app.renderer.view.style.position = 'absolute';
app.renderer.view.style.display = 'block';

app.renderer.resize(window.innerWidth, window.innerHeight);
window.onresize = () => app.renderer.resize(window.innerWidth, window.innerHeight);

// Aliases
const resources = Loader.resources;

const imageSources = {
	bulbasaur: './src/images/bulbasaur.json',
};

const resourceUrls = Object.keys(imageSources).map((key) => imageSources[key]);

const sprites = {
	bulbasaur: {} as Sprite,
};

Loader.add(resourceUrls)
	.on('progress', (loader, resource): void => {
		console.log(`Loading... ${loader.progress}%`);
		console.log(resource.url);
		console.log(loader, resource);
	})
	.load((): void => {

		sprites.bulbasaur = Sprite.fromFrame('bulbasaur_walk_down_01' );

		sprites.bulbasaur.interactive = true;
		sprites.bulbasaur.buttonMode = true;

		sprites.bulbasaur.scale.set(4);
		sprites.bulbasaur.position.set(200, 200);
		app.stage.addChild(sprites.bulbasaur);

		app.ticker.add(render);
	});

const bulbasaurSpeed = {
	rot: 0,
	scale: 0,
};

const render = (dt) => {
	if ( (bulbasaurSpeed.rot * dt) >= 360) {
		bulbasaurSpeed.rot = 0;
	}
	if ( (bulbasaurSpeed.scale * dt) >= 360) {
		bulbasaurSpeed.scale = 0;
	}

	sprites.bulbasaur.position.y += 4 * dt;
	sprites.bulbasaur.scale.y = 4 + Math.sin((bulbasaurSpeed.scale * dt) * ((Math.PI * 2) / 360)) / 2;
	sprites.bulbasaur.rotation = Math.sin( ( (bulbasaurSpeed.rot * dt) * ((Math.PI * 2) / 360) ) ) / 4;

	if (sprites.bulbasaur.position.y > app.renderer.height ) {
		sprites.bulbasaur.position.y = 1 - sprites.bulbasaur.height;
	}

	if ( sprites.bulbasaur.rotation < -0.2
		&& sprites.bulbasaur.texture.textureCacheIds[0] !== 'bulbasaur_walk_down_02') {
		sprites.bulbasaur.texture = PIXI.Texture.fromFrame('bulbasaur_walk_down_02');
		console.log('texture 2');
	} else if (sprites.bulbasaur.rotation > 0.2
		&& sprites.bulbasaur.texture.textureCacheIds[0] !== 'bulbasaur_walk_down_01') {
		sprites.bulbasaur.texture = PIXI.Texture.fromFrame('bulbasaur_walk_down_01');
		console.log('texture 1');
	}

	bulbasaurSpeed.scale += 20;
	bulbasaurSpeed.rot += 10;
};
