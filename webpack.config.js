module.exports = {
	mode: 'production',
	devtool: "inline-source-map",
	entry: './src/app.ts',
	output: {
		filename: 'bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				loader: 'ts-loader'
			},
		]
	},
	resolve: {
		extensions: [".tsx", ".ts", ".js"]
	}
};
